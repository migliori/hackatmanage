<?php
include_once "bd.inc.php";

if ( $_SERVER["SCRIPT_FILENAME"] == __FILE__ ){
    $root="..";
}
require_once("$root/model/Membre.php");

//Récupération d'un Membre

function getUser(int $id) : Membre{
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select m.id, lastname, firstname, phone, email from membre m inner join \"user\" u on m.id = u.idmembre where m.id  = :id;");
        $req->bindParam(':id', $id);
        $req->execute();
        $req->setFetchMode(PDO::FETCH_CLASS, 'Membre');

        $membre = $req->fetch();

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }

    return $membre;
}

//Récupération du Membre inscrit

function getMembresWithoutEquipeHackathon(int $idHackathon) : array{
    $resultat = array();
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select *
                                    from membre
                                    inner join inscription i on i.idmembre = membre.id
                                    where idequipe is null and idhackathon = :idHackathon;");
        $req->bindParam(':idHackathon', $idHackathon);
        $req->execute();
        $req->setFetchMode(PDO::FETCH_CLASS, 'Membre');

        while ($membre = $req->fetch()){
            $resultat[] = $membre;
        }

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

//Renvoie un boolean qui vérifie si l'animateur peut manager l'équipe

function hasEquipeAnimateur(Membre $membre, int $idEquipe) : bool{

    $bool = false;

    foreach($membre->getLesEquipesAnimateur() as $equipe){
        if($equipe['id'] == $idEquipe){
            $bool = true;
        }
    }
    return $bool;
}

//Renvoie un boolean qui vérifie si l'animateur peut manager le projet

function hasProjectAnimateur(Membre $membre, int $idProjet) : bool{

    $bool = false;

    foreach($membre->getLesProjectsAnimateur() as $project){
        if($project->getId() == $idProjet){
            $bool = true;
        }
    }
    return $bool;
}

//Renvoie un boolean qui vérifie si l'animateur peut manager le hackathon

function hasHackathonAnimateur(Membre $membre, int $idHackathon) : bool {

    $bool = false;

    foreach($membre->getLesHackathonsAnimateur() as $hackathon){
        if($hackathon->getId() == $idHackathon){
            $bool = true;
        }
    }
    return $bool;
}

if ($_SERVER["SCRIPT_FILENAME"] == __FILE__) {
    // prog principal de test
    header('Content-Type:text/plain');

    echo "getMember(8) : \n";
    print_r(getUser(8));

    echo "getMembresWithoutEquipe(2) : \n";
    print_r(getMembresWithoutEquipe(2));
}