<?php
include_once "bd.inc.php";

if ( $_SERVER["SCRIPT_FILENAME"] == __FILE__ ){
    $root="..";
}
require_once("$root/model/Membre.php");
require_once ("$root/dal/bd.hackathon.inc.php");
require_once ("$root/dal/bd.project.inc.php");
require_once ("$root/dal/bd.equipe.inc.php");

//Verifier si le mot de passe est correct

function verifMdp(string $email, string $password) : bool{

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare('select id, password from "user" where email = :email');
        $req->bindParam(':email', $email);
        $req->execute();
        $mdp = $req->fetch();

        $bool = false;
        if($mdp != false)
        {
            if($mdp["password"] == null)
            {
                createPassToUser($password, $mdp["id"]);

                $bool = true;
            }
            else
            {
                if(password_verify($password, $mdp['password']))
                {
                    $bool = true;
                }
            }
        }
        else
        {
            $_SESSION["error"] = "Identifiant ou mot de passe incorrect !";
        }

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $bool;
}

//Génère le hash du mot de passe -> ajout en base de données

function createPassToUser(string $password, int $id) : void
{
    $hashPassword = password_hash($password, PASSWORD_ARGON2I);

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare('UPDATE "user" SET password = :password WHERE id = :id');
        $req->bindParam(':password',$hashPassword);
        $req->bindParam(':id',$id);
        $req->execute();
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
}

//Récupération de l'utilisateur pour le transformer en Membre

function getMember(string $email,string $password) : ?Membre{

    $membre = null;

    if(verifMdp($email, $password))
    {
        try {
            $cnx = connexionPDO();
            $req = $cnx->prepare('SELECT  m.id, m.lastname, m.firstname, m.phone, m."type", u.email, u."password"
                                            FROM membre m
                                            INNER JOIN "user" u on m.id = u.idmembre
                                            WHERE u.email= :email
                                            ');
            $req->bindParam(':email', $email);
            $req->execute();

            $req->setFetchMode(PDO::FETCH_CLASS, 'Membre');
            $membre = $req->fetch();

        } catch (PDOException $e) {
            print "Erreur !: " . $e->getMessage();
            die();
        }
    }
    return $membre;
}

//Vérification du rôle du Membre

function verifRole(Membre $membre){

    $bool = false;

    try {
        $id = $membre->getId();

        $cnx = connexionPDO();
        $expert = $cnx->prepare("select id from expert where id = :id");
        $expert->bindParam(':id', $id);
        $expert->execute();

        $jury = $cnx->prepare("select id from jury where id = :id");
        $jury->bindParam(':id', $id);
        $jury->execute();

        $animateur = $cnx->prepare("select id from animateur where id = :id");
        $animateur->bindParam(':id', $id);
        $animateur->execute();

        if($animateur->fetchAll() != null )
        {
            $_SESSION["role"] = "Animateur";
            $membre->setLesHackathonsAnimateur(getHackathonsAnimateur($membre));
            $membre->setLesProjectsAnimateur(getProjectsAnimateur($membre));
            $membre->setLesEquipesAnimateur(getEquipesAnimateur($membre));
            $bool = true;
        }
        else if($expert->fetchAll() != null || $jury->fetchAll() != null)
        {
            $membre->setLesHackathonsJury(getHackathonsJury($membre));
            $membre->setLesProjectsJury(getProjectsJury($membre));
            $membre->setLesHackathonsExpert(getHackathonsExpert($membre));
            $membre->setLesProjectsExpert(getProjectsExpert($membre));
            $_SESSION["role"] = "Jury ou Expert";
            $bool = true;
        }

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }

    return $bool;
}

if ($_SERVER["SCRIPT_FILENAME"] == __FILE__) {
    // prog principal de test
    header('Content-Type:text/plain');

    echo "verifUser() : \n";
    print_r(verifUser("jGoldman@gmail.com","test"));
}