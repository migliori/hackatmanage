<?php
include_once "bd.inc.php";

if ( $_SERVER["SCRIPT_FILENAME"] == __FILE__ ){
    $root="..";
}
require_once("$root/model/Equipe.php");

//Récupérations de la liste des équipes

function getEquipes() : array {
    $resultat = array();

    try{
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from equipe");
        $req->execute();

        while($equipe = $req->fetch()) {
            $resultat[] = $equipe;
        }
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

//Récupération d'une équipe

function getEquipe(int $id) : array {
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from equipe where id = :id");
        $req->bindParam(':id', $id);
        $req->execute();


        $projet = [];
        if ($res = $req->fetch()){
            $projet = $res;
        }


    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $projet;
}

//Récupère les équipes liées a un projet

function getEquipeByProjet(int $id) : array
{
    $resultat = array();
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select id, nom from equipe where idprojet = :id");
        $req->bindParam(':id', $id);
        $req->execute();

        while ($equipe = $req->fetch()){
            $resultat[] = $equipe;
        }

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

//Récupère les équipes dont l'animateur gestionne

function getEquipesAnimateur(Membre $membre) : array{
    $resultat = array();
    $mesHackathons = $membre->getLesHackathonsAnimateur();
    $idMembre = $membre->getId();

    try{
        $cnx = connexionPDO();
        foreach($mesHackathons as $hackathon){
            $req = $cnx->prepare("select e.id, e.nom, e.idprojet
                                        from equipe e
                                        inner join projet p on p.id = e.idprojet
                                        join hackathon h on h.id = p.idhackathon
                                        join animateurcompose a on a.idhackathon = h.id
                                        where p.idhackathon = :idHackathon and a.idanimateur = :idAnimateur");
            $idHackathon = $hackathon->getId();
            $req->bindParam(':idHackathon', $idHackathon);
            $req->bindParam(':idAnimateur', $idMembre);
            $req->execute();

            while ($equipe = $req->fetch()) {
                $resultat[] = $equipe;
            }
        }

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

//Récupération des inscrits pour une équipe

function getInscritsEquipe(int $id) : array {
    $resultat = array();

    try{
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from inscription where idequipe = :id");
        $req->bindParam(':id', $id);
        $req->execute();

        while ($inscrit = $req->fetch()) {
            $resultat[] = $inscrit;
        }

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

//Créer une équipe

function insertEquipe(string $nom, int $idprojet)
{
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("insert into equipe(nom, idprojet) values (:nom, :idprojet)");
        $req->bindParam(':nom', $nom);
        $req->bindParam('idprojet', $idprojet);
        $req->execute();

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
}

//Ajouter un Membre à une Equipe

function insertMembreIntoEquipe(int $idEquipe, int $idMembre)
{
    try{
        $cnx = connexionPDO();
        $req = $cnx->prepare("update inscription
                                    set idequipe = :idEquipe
                                    where id = :idMembre");
        $req->bindParam(':idEquipe', $idEquipe);
        $req->bindParam('idMembre', $idMembre);
        $req->execute();

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
}

function insertChiefIntoEquipe(int $idEquipe, int $idInscrit)
{
    try{
        $cnx = connexionPDO();
        $req = $cnx->prepare("update equipe 
                                    set idchef = :idInscrit
                                    where id = :idEquipe");
        $req->bindParam(':idInscrit', $idInscrit);
        $req->bindParam(':idEquipe', $idEquipe);
        $req->execute();

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
}

if ($_SERVER["SCRIPT_FILENAME"] == __FILE__) {
    // prog principal de test
    header('Content-Type:text/plain');

    echo "getEquipes() : \n";
    print_r(getEquipes());

//    echo "getEquipe(1) : \n";
//    print_r(getEquipe(1));

//    echo "getInscritsEquipe(1) : \n";
//    print_r(getInscritsEquipe(1));

//    echo "getEquipeByMemberHackathon(3) : \n";
//    print_r(getEquipeByHackathon(3));
}