INSERT INTO membre (lastname, firstname, phone, "type")
VALUES ('Ritou', 'Lucas', '06 06 06 06 14', 'jury'),
       ('Goldman', 'Jean-Jacques', '06 06 06 06 07', 'animateur'),
       ('Rufino', 'Hugo', '06 06 06 06 08', 'jury'),
       ('Calas', 'Dorian', '06 06 06 06 09', 'animateur'),
       ('Test', 'Test', NULL, 'participant'),
       ('Er', 'Re', NULL, 'participant'),
       ('Situation', 'Lena', '06 06 06 06 10', 'participant'),
       ('Abilpue', 'Cesar', '06 06 06 06 11', 'participant'),
       ('Michou', 'Elsa', '06 06 06 06 13', 'expert'),
       ('Quinn', 'Eva', '06 06 06 06 12', 'animateur');

INSERT INTO "user" (email, "password", roles, idmembre)
VALUES ('jGoldman@gmail.com',
        '$argon2i$v=19$m=65536,t=4,p=1$UTVGRXNmRTgxTy5MVmpDQg$ktejGZNqLrVGB9tUV/9+HAM1gdY5sLqYywvtmtzZAZQ',
        '["ROLE_USER"]', 1),
       ('sLena@gmail.com',
        '$argon2i$v=19$m=65536,t=4,p=1$UTVGRXNmRTgxTy5MVmpDQg$ktejGZNqLrVGB9tUV/9+HAM1gdY5sLqYywvtmtzZAZQ',
        '["ROLE_USER"]', 5),
       ('aCesar@gmail.com',
        '$argon2i$v=19$m=65536,t=4,p=1$UTVGRXNmRTgxTy5MVmpDQg$ktejGZNqLrVGB9tUV/9+HAM1gdY5sLqYywvtmtzZAZQ',
        '["ROLE_USER"]', 6),
       ('dCalas@gmail.com',
        '$argon2i$v=19$m=65536,t=4,p=1$UTVGRXNmRTgxTy5MVmpDQg$ktejGZNqLrVGB9tUV/9+HAM1gdY5sLqYywvtmtzZAZQ',
        '["ROLE_USER"]', 3),
       ('lRitou@gmail.com',
        '$argon2i$v=19$m=65536,t=4,p=1$UTVGRXNmRTgxTy5MVmpDQg$ktejGZNqLrVGB9tUV/9+HAM1gdY5sLqYywvtmtzZAZQ',
        '["ROLE_USER"]', 4),
       ('qEva@gmail.com',
        '$argon2i$v=19$m=65536,t=4,p=1$UTVGRXNmRTgxTy5MVmpDQg$ktejGZNqLrVGB9tUV/9+HAM1gdY5sLqYywvtmtzZAZQ',
        '["ROLE_USER"]', 7),
       ('mElsa@gmail.com',
        '$argon2i$v=19$m=65536,t=4,p=1$UTVGRXNmRTgxTy5MVmpDQg$ktejGZNqLrVGB9tUV/9+HAM1gdY5sLqYywvtmtzZAZQ',
        '["ROLE_USER"]', 8),
       ('hRufino@gmail.com',
        '$argon2i$v=19$m=65536,t=4,p=1$UTVGRXNmRTgxTy5MVmpDQg$ktejGZNqLrVGB9tUV/9+HAM1gdY5sLqYywvtmtzZAZQ',
        '["ROLE_USER"]', 2),
       ('t@eest.fr', NULL, '["ROLE_USER"]', 9),
       ('re@re.r', NULL, '["ROLE_USER"]', 10);

INSERT INTO organisateur (id)
VALUES (3),
       (6),
       (8);

INSERT INTO typeinscription (libelle)
VALUES ('Sur validation'),
       ('Automatique');

INSERT INTO hackathon ("name", "date", "location", target, topic, description, nbentrant, idtypeinscription, canvote,
                       caninscription, canposition, idorganisateur)
VALUES ('Hack''Web', '2037-03-03', 'Nantes', 'développement durable', 'Citoyenneté Mondiale',
        'Ce hackathon sur la Citoyenneté Mondiale, ouverts aux jeunes, doit permettre d’échanger autour de projets concrets répondant aux objectifs de développement durable de l’ONU.',
        19, 1, 0, 0, 0, 3),
       ('Hackact', '2037-03-03', 'Nantes', 'développement durable', 'Citoyenneté Mondiale',
        'Ce hackathon sur la Citoyenneté Mondiale, ouverts aux jeunes, doit permettre d’échanger autour de projets concrets répondant aux objectifs de développement durable de l’ONU.',
        19, 1, 0, 0, 0, 3),
       ('Hack''Orga', '2444-06-03', 'Nantes', 'développement durable', 'Citoyenneté Mondiale',
        'Ce hackathon sur la Citoyenneté Mondiale, ouverts aux jeunes, doit permettre d’échanger autour de projets concrets répondant aux objectifs de développement durable de l’ONU.s',
        44, 2, 0, 0, 0, 3),
       ('Hack''Innov', '2076-01-01', 'Nantes', 'service publique', 'Services publics et Innovation',
        'Ce hackathon vise à réunir les citoyens d’une communauté pour développer des solutions afin d’améliorer les services publics offerts aux citoyens',
        15, 2, 0, 0, 0, 8),
       ('Hack''Consul', '2078-11-07', 'Nantes', 'métier consultant', 'Réinventer un métier',
        'Ce hackathon vise à réunir des étudiants afin de réinventer le métier de Consultant', 9, 1, 0, 0, 0, 6);

INSERT INTO animateur (id)
VALUES (1),
       (3),
       (7),
       (10);

INSERT INTO animateurcompose (idanimateur, idhackathon)
VALUES (1, 3),
       (3, 4),
       (7, 2);

INSERT INTO expert (id, job)
VALUES (8, 'Chargé de communication');

INSERT INTO expertcompose (idexpert, idhackathon)
VALUES (8, 5);

INSERT INTO jury (id)
VALUES (2),
       (4),
       (9);

INSERT INTO jurycompose (idjury, idhackathon)
VALUES (2, 3),
       (4, 4);

INSERT INTO participant (id)
VALUES (6),
       (5);

INSERT INTO projet (libelle, description, isvalid, idhackathon)
VALUES ('la barbe de troie', 'Description du projet "la barbe de troie"', 0, 1),
       ('courir avant d''atteindre le cheval', 'Description du projet "courir avant d''atteindre le cheval"', 0, 4),
       ('les métiers de l''horticulture avec une base de données',
        'Description du projet "les métiers de l''horticulture avec une base de données"', 0, 5),
       ('l''exemple de la bouilloire',
        'Quand vous êtes harassés de fatigue, pensez toujours à l’exemple de la bouilloire. Elle a beau avoir le couvercle en ébullition, cela ne l’empêche pas de chanter.',
        0, 2),
       ('base du coton',
        'Le coton est une fibre végétale qui entoure les graines des cotonniers « véritables » (Gossypium sp.), arbustes de la famille des Malvacées. Cette fibre, constituée de cellulose presque pure, est généralement transformée en fil qui est tissé pour fabriquer des tissus. Le coton est la fibre naturelle la plus produite dans le monde, principalement en Chine et en Inde. Depuis le xixe siècle, il constitue, grâce aux progrès de l''industrialisation et de l''agronomie, la première fibre textile du monde (près de la moitié de la consommation mondiale de fibres textiles)',
        0, 3),
       ('masquer les coups dans la statue',
        'La sculpture sur pierre est une technique particulière de sculpture en ce qu''elle désigne un mode de fabrication d''une œuvre différente du modelage en argile ou en fonte. Le terme ne doit pas être confondu avec l''activité des tailleurs de pierre qui façonnent des blocs de pierre destinés à la sculpture mais aussi à l''architecture, le bâtiment ou le génie civil. C''est aussi une expression utilisée par les archéologues, les historiens et anthropologues pour décrire la fabrication des pétroglyphe.',
        0, 2),
       ('les réflexions transitives', 'Description du projet "les reflexions transitives"', 0, 1);

INSERT INTO equipe (nom, idprojet, idchef)
VALUES ('migliori', 4, NULL),
       ('katcentkat', 5, NULL),
       ('l''attaque des titouans', 7, NULL);


INSERT INTO inscription (dateinscription, isvalid, idmembre, idhackathon, idprojet, idequipe)
VALUES ('2022-02-28', 0, 5, 1, 4, 1),
       ('2021-11-24', 0, 6, 2, 7, 3);
INSERT INTO vote (idexpert, idprojet)
VALUES (8, 7);