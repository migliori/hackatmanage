-- migliori.membre definition

-- Drop table

-- DROP TABLE migliori.membre;

CREATE TABLE membre
(
    id        serial4      NOT NULL,
    lastname  varchar(255) NOT NULL,
    firstname varchar(255) NOT NULL,
    phone     varchar(255) NULL DEFAULT NULL :: character varying,
    "type"    varchar      NOT NULL DEFAULT 'participant':: character varying,
    CONSTRAINT membre_pkey PRIMARY KEY (id)
);

-- migliori.typeinscription definition

-- Drop table

-- DROP TABLE migliori.typeinscription;

CREATE TABLE typeinscription
(
    id      serial4      NOT NULL,
    libelle varchar(255) NOT NULL,
    CONSTRAINT typeinscription_pkey PRIMARY KEY (id)
);


-- migliori.animateur definition

-- Drop table

-- DROP TABLE migliori.animateur;

CREATE TABLE animateur
(
    id int4 NOT NULL,
    CONSTRAINT animateur_pkey PRIMARY KEY (id),
    CONSTRAINT animateur_id_fkey FOREIGN KEY (id) REFERENCES membre (id)
);


-- migliori.expert definition

-- Drop table

-- DROP TABLE migliori.expert;

CREATE TABLE expert
(
    id  int4         NOT NULL,
    job varchar(255) NOT NULL,
    CONSTRAINT expert_pkey PRIMARY KEY (id),
    CONSTRAINT expert_id_fkey FOREIGN KEY (id) REFERENCES membre (id)
);


-- migliori.jury definition

-- Drop table

-- DROP TABLE migliori.jury;

CREATE TABLE jury
(
    id int4 NOT NULL,
    CONSTRAINT jury_pkey PRIMARY KEY (id),
    CONSTRAINT jury_id_fkey FOREIGN KEY (id) REFERENCES membre (id)
);


-- migliori.organisateur definition

-- Drop table

-- DROP TABLE migliori.organisateur;

CREATE TABLE organisateur
(
    id int4 NOT NULL,
    CONSTRAINT organisateur_pkey PRIMARY KEY (id),
    CONSTRAINT organisateur_id_fkey FOREIGN KEY (id) REFERENCES membre (id)
);


-- migliori.participant definition

-- Drop table

-- DROP TABLE migliori.participant;

CREATE TABLE participant
(
    id int4 NOT NULL,
    CONSTRAINT participant_pkey PRIMARY KEY (id),
    CONSTRAINT participant_id_fkey FOREIGN KEY (id) REFERENCES membre (id)
);


-- migliori."user" definition

-- Drop table

-- DROP TABLE migliori."user";

CREATE TABLE "user"
(
    id         serial4      NOT NULL,
    email      varchar(255) NOT NULL,
    "password" varchar(255) NULL,
    roles      json NULL,
    idmembre   int4 NULL,
    CONSTRAINT test_pkey PRIMARY KEY (id),
    CONSTRAINT user_idmembre_fkey FOREIGN KEY (idmembre) REFERENCES membre (id)
);


-- migliori.hackathon definition

-- Drop table

-- DROP TABLE migliori.hackathon;

CREATE TABLE hackathon
(
    id                serial4      NOT NULL,
    "name"            varchar(255) NOT NULL,
    "date"            date         NOT NULL,
    "location"        varchar(255) NOT NULL,
    target            varchar(255) NOT NULL,
    topic             text         NOT NULL,
    description       text         NOT NULL,
    nbentrant         int4         NOT NULL,
    idtypeinscription int4         NOT NULL,
    canvote           int2         NOT NULL DEFAULT '0':: smallint,
    caninscription    int2         NOT NULL DEFAULT '0':: smallint,
    canposition       int2         NOT NULL DEFAULT '0':: smallint,
    idorganisateur    int4         NOT NULL,
    CONSTRAINT hackathon_pkey PRIMARY KEY (id),
    CONSTRAINT hackathon_idorganisateur_fkey FOREIGN KEY (idorganisateur) REFERENCES organisateur (id),
    CONSTRAINT hackathon_idtypeinscription_fkey FOREIGN KEY (idtypeinscription) REFERENCES typeinscription (id)
);

-- migliori.jurycompose definition

-- Drop table

-- DROP TABLE migliori.jurycompose;

CREATE TABLE jurycompose
(
    idjury      int4 NOT NULL,
    idhackathon int4 NOT NULL,
    CONSTRAINT jurycompose_pkey PRIMARY KEY (idhackathon, idjury),
    CONSTRAINT jurycompose_idhackathon_fkey FOREIGN KEY (idhackathon) REFERENCES hackathon (id),
    CONSTRAINT jurycompose_idjury_fkey FOREIGN KEY (idjury) REFERENCES jury (id)
);


-- migliori.projet definition

-- Drop table

-- DROP TABLE migliori.projet;

CREATE TABLE projet
(
    id          serial4      NOT NULL,
    libelle     varchar(255) NOT NULL,
    description text         NOT NULL,
    isvalid     int2         NOT NULL DEFAULT '0':: smallint,
    idhackathon int4         NOT NULL,
    CONSTRAINT projet_pkey PRIMARY KEY (id),
    CONSTRAINT projet_idhackathon_fkey FOREIGN KEY (idhackathon) REFERENCES hackathon (id)
);


-- migliori.vote definition

-- Drop table

-- DROP TABLE migliori.vote;

CREATE TABLE vote
(
    idexpert int4 NOT NULL,
    idprojet int4 NOT NULL,
    CONSTRAINT vote_pkey PRIMARY KEY (idexpert, idprojet),
    CONSTRAINT vote_idexpert_fkey FOREIGN KEY (idexpert) REFERENCES expert (id),
    CONSTRAINT vote_idprojet_fkey FOREIGN KEY (idprojet) REFERENCES projet (id)
);


-- migliori.animateurcompose definition

-- Drop table

-- DROP TABLE migliori.animateurcompose;

CREATE TABLE animateurcompose
(
    idanimateur int4 NOT NULL,
    idhackathon int4 NOT NULL,
    CONSTRAINT animateurcompose_pkey PRIMARY KEY (idanimateur, idhackathon),
    CONSTRAINT animateurcompose_idanimateur_fkey FOREIGN KEY (idanimateur) REFERENCES animateur (id),
    CONSTRAINT animateurcompose_idhackathon_fkey FOREIGN KEY (idhackathon) REFERENCES hackathon (id)
);


-- migliori.expertcompose definition

-- Drop table

-- DROP TABLE migliori.expertcompose;

CREATE TABLE expertcompose
(
    idexpert    int4 NOT NULL,
    idhackathon int4 NOT NULL,
    CONSTRAINT expertcompose_pkey PRIMARY KEY (idhackathon, idexpert),
    CONSTRAINT expertcompose_idexpert_fkey FOREIGN KEY (idexpert) REFERENCES expert (id),
    CONSTRAINT expertcompose_idhackathon_fkey FOREIGN KEY (idhackathon) REFERENCES hackathon (id)
);


-- migliori.equipe definition

-- Drop table

-- DROP TABLE migliori.equipe;

CREATE TABLE equipe
(
    id       serial4      NOT NULL,
    nom      varchar(255) NOT NULL,
    idprojet int4 NULL,
    idchef   int4 NULL,
    CONSTRAINT equipe_pkey PRIMARY KEY (id)
);


-- migliori.inscription definition

-- Drop table

-- DROP TABLE migliori.inscription;

CREATE TABLE inscription
(
    id              serial4 NOT NULL,
    dateinscription date    NOT NULL,
    isvalid         int2    NOT NULL DEFAULT '0':: smallint,
    idmembre        int4    NOT NULL,
    idhackathon     int4    NOT NULL,
    idprojet        int4 NULL,
    idequipe        int4 NULL,
    CONSTRAINT inscription_pkey PRIMARY KEY (id)
);

-- migliori.equipe foreign keys

ALTER TABLE equipe
    ADD CONSTRAINT equipe_idchef_fkey FOREIGN KEY (idchef) REFERENCES inscription (id);
ALTER TABLE equipe
    ADD CONSTRAINT equipe_idprojet_fkey FOREIGN KEY (idprojet) REFERENCES projet (id);


-- migliori.inscription foreign keys

ALTER TABLE inscription
    ADD CONSTRAINT inscription_idequipe_fkey FOREIGN KEY (idequipe) REFERENCES equipe (id);
ALTER TABLE inscription
    ADD CONSTRAINT inscription_idhackathon_fkey FOREIGN KEY (idhackathon) REFERENCES hackathon (id);
ALTER TABLE inscription
    ADD CONSTRAINT inscription_idmembre_fkey1 FOREIGN KEY (idmembre) REFERENCES participant (id);
ALTER TABLE inscription
    ADD CONSTRAINT inscription_idprojet_fkey FOREIGN KEY (idprojet) REFERENCES projet (id);