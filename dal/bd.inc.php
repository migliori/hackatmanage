<?php

function connexionPDO() : PDO {

    global $root;

    try {
        $ini = parse_ini_file("$root/env/db.ini");

        $server = $ini['db_address'];
        $port = $ini['db_port'];
        $db =  $ini['db_name'];
        $user = $ini['db_user'];
        $pwd = $ini['db_password'];

        $conn = new PDO("pgsql:host=$server;port=$port;dbname=$db", $user, $pwd);

        $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        return $conn;
    } catch (PDOException $e) {
        print "Erreur de connexion PDO ";
        die();
    }
}

if ($_SERVER["SCRIPT_FILENAME"] == __FILE__) {
    // prog de test
    header('Content-Type:text/plain');

    echo "connexionPDO() : \n";
    print_r(connexionPDO());
}

