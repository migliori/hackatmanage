<?php
include_once "bd.inc.php";

if ( $_SERVER["SCRIPT_FILENAME"] == __FILE__ ){
    $root="..";
}
require_once("$root/dal/bd.authentification.inc.php");
require_once("$root/model/Project.php");

//Récupération de tout les projets

function getProjects() : array {
    $resultat = array();

    try{
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from projet");
        $req->execute();
        $req->setFetchMode(PDO::FETCH_CLASS, 'Project');

        while ($project = $req->fetch()){
            $resultat[] = $project;
        }

    } catch (PDOException $e){
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

//Récupération d'un projet

function getProject(int $id) : ?array {
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from projet where id = :id");
        $req->bindParam(':id', $id);
        $req->execute();

        $projet = null;
        if ($res = $req->fetch()){
            $projet = $res;
        }

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $projet;
}

//Récupération des projets liés à un Membre de type Animateur

function getProjectsAnimateur(Membre $membre) : array{
    $resultat = array();

    try{
        $cnx = connexionPDO();
        $req = $cnx->prepare("select p.id, p.libelle, p.isvalid, p.idhackathon
                                     from projet p
                                     inner join hackathon h on h.id = p.idhackathon
                                     join animateurcompose a on a.idhackathon = h.id                                                                        
                                     where idanimateur = :id");
        $id = $membre->getId();
        $req->bindParam(':id', $id);
        $req->execute();
        $req->setFetchMode(PDO::FETCH_CLASS, 'Project');

        while ($project = $req->fetch()){
            $resultat[] = $project;
        }

    } catch (PDOException $e){
        print "Erreur !: " .$e->getMessage();
        die();
    }
    return $resultat;
}

//Récupération des projets liés à un Membre de type Expert

function getProjectsExpert(Membre $membre) : array{
    $resultat = array();

    try{
        $cnx = connexionPDO();
        $req = $cnx->prepare("select p.id, p.libelle, p.isvalid, p.idhackathon
                                     from projet p
                                     inner join hackathon h on h.id = p.idhackathon
                                     join expertcompose e on e.idhackathon = h.id                                                                        
                                     where idexpert = :id");
        $id = $membre->getId();
        $req->bindParam(':id', $id);
        $req->execute();
        $req->setFetchMode(PDO::FETCH_CLASS, 'Project');

        while ($project = $req->fetch()){
            $resultat[] = $project;
        }

    } catch (PDOException $e){
        print "Erreur !: " .$e->getMessage();
        die();
    }
    return $resultat;
}

//Récupération des projets liés à un Membre de type Jury

function getProjectsJury(Membre $membre) : array{
    $resultat = array();

    try{
        $cnx = connexionPDO();
        $req = $cnx->prepare("select p.id, p.libelle, p.isvalid, p.idhackathon
                                     from projet p
                                     inner join hackathon h on h.id = p.idhackathon
                                     join jurycompose j on j.idhackathon = h.id                                                                        
                                     where idjury = :id");
        $id = $membre->getId();
        $req->bindParam(':id', $id);
        $req->execute();
        $req->setFetchMode(PDO::FETCH_CLASS, 'Project');

        while ($project = $req->fetch()){
            $resultat[] = $project;
        }

    } catch (PDOException $e){
        print "Erreur !: " .$e->getMessage();
        die();
    }
    return $resultat;
}

//Validation du projet retenu

function setProjectIsValid(int $idProjet) : void {
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("update projet set isvalid = 1 where id = :idProjet");
        $req->bindParam(':idProjet', $idProjet);
        $req->execute();

    } catch (PDOException $e){
        print "Erreur !: " . $e->getMessage();
        die();
    }
}

//Remise à zéro de toutes les validations d'un Hackathon

function clearIsValid(int $idHackathon) : void {
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("UPDATE projet SET isvalid = 0 WHERE idhackathon = :idHackathon");
        $req->bindParam(':idHackathon', $idHackathon);
        $req->execute();

    } catch (PDOException $e){
        print "Erreur !: " . $e->getMessage();
        die();
    }
}

if ($_SERVER["SCRIPT_FILENAME"] == __FILE__) {
    // prog principal de test
    header('Content-Type:text/plain');

    echo "getProjects() : \n";
    print_r(getProjects());

    echo "getProjectByMember(1) : \n";
    print_r(getProjectsAnimateur(1));

    echo "getProject(2) : \n";
    print_r(getProject(2));
}