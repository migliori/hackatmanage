<?php
include_once "bd.inc.php";

if ( $_SERVER["SCRIPT_FILENAME"] == __FILE__ ){
    $root="..";
}

require_once("$root/model/Membre.php");
require_once("$root/model/Inscription.php");
require_once("$root/model/Hackathon.php");
require_once("$root/model/Registration.php");

//Récupérations des hackathons

function getHackathons() : array {
    $resultat = array();

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from hackathon");
        $req->execute();
        $req->setFetchMode(PDO::FETCH_CLASS,'Hackathon');

        while ($hackathon = $req->fetch()) {
            $resultat[] = $hackathon;
        }
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

//Récupération d'un hackathon

function getHackathon(int $id) : ?Hackathon {
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from hackathon where id = :id");
        $req->bindParam(':id',$id);
        $req->execute();

        $req->setFetchMode(PDO::FETCH_CLASS,'Hackathon');

        $hackathon = null;
        if ($res = $req->fetch()){
            $hackathon = $res;
        }

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $hackathon;
}

//Enregistrement des inscriptions

function insertRegistration(int $idHackathon, string $firstname, string $lastname)
{
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("insert into registration(idhackathon, firstname, lastname) values (:idhackathon,:firstname, :lastname)");
        $req->bindParam(':idhackathon',$idHackathon);
        $req->bindParam(':firstname',$firstname);
        $req->bindParam(':lastname',$lastname);
        $req->execute();
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
}

//Récupère les hackathons liées à un Membre de type Animateur

function getHackathonsAnimateur(Membre $membre) : ?array
{
    $resultat = array();
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from hackathon inner join animateurcompose a on hackathon.id = a.idhackathon where idanimateur = :id");
        $id = $membre->getId();
        $req->bindParam(':id', $id);
        $req->execute();

        $req->setFetchMode(PDO::FETCH_CLASS,'Hackathon');
        while ($hackathons = $req->fetch())
        {
            $resultat[] = $hackathons;
        }

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }

    return $resultat;
}

//Récupère les hackathons liées à un Membre de type Expert

function getHackathonsExpert(Membre $membre) : array
{
    $resultat = array();
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from hackathon inner join expertcompose e on hackathon.id = e.idhackathon where idexpert = :id");
        $id = $membre->getId();
        $req->bindParam(':id', $id);
        $req->execute();

        $req->setFetchMode(PDO::FETCH_CLASS,'Hackathon');
        while ($hackathons = $req->fetch())
        {
            $resultat[] = $hackathons;
        }

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }

    return $resultat;
}

//Récupère les hackathons liées à un Membre de type Jury

function getHackathonsJury(Membre $membre) : array
{
    $resultat = array();
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select * from hackathon inner join jurycompose j on hackathon.id = j.idhackathon where idjury = :id");
        $id = $membre->getId();
        $req->bindParam(':id', $id);
        $req->execute();

        $req->setFetchMode(PDO::FETCH_CLASS,'Hackathon');
        while ($hackathons = $req->fetch())
        {
            $resultat[] = $hackathons;
        }

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }

    return $resultat;
}

//Récupération des inscrits d'un hackathon

function getInscriptionsByHackathons(int $idHackathon) : array {
    $resultat = array();

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select m.id, m.lastname, m.firstname, m.phone, u.email, i.id, i.dateinscription, i.isvalid from membre m inner join \"user\" u on m.id = u.idmembre inner join inscription i on m.id = i.idmembre inner join hackathon h on h.id = i.idhackathon where idhackathon = :idhackathon;");
        $req->bindParam(':idhackathon',$idHackathon);
        $req->execute();

        while ($inscriptions = $req->fetch()) {
            $inscriptions["isvalid"] = ($inscriptions["isvalid"] == 1) ? "True":"False";
            $resultat[] = $inscriptions;
        }

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
    return $resultat;
}

//Récupération des projets proposées d'un hackathon

function getLesProjetProposer(int $idhackathon) : array
{
    $projects = array();

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare('select p.id, p.libelle, p.isvalid, h.name from projet p inner join hackathon h on h.id = p.idhackathon where p.idhackathon = :id');
        $req->bindParam(":id", $idhackathon);
        $req->execute();

        while ($hackathons = $req->fetch())
        {
            $projects[] = $hackathons;
        }

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }

    return $projects;
}

//Récupération des projets voter par un Membre de type Expert pour un Hackathon

function getProjetVote(int $idHackathon, int $idUser)
{
    $projects = array();

    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select v.idProjet from vote v inner join projet p on p.id = v.idprojet where idhackathon = :idHackathon and idexpert = :idExpert");
        $req->bindParam(":idExpert", $idUser);
        $req->bindParam(":idHackathon", $idHackathon);
        $req->execute();

        while ($hackathons = $req->fetch())
        {
            $projects[] = $hackathons;
        }

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }

    return $projects;
}

//Récupération des projets qui ont été proposés

function getLesProjetProposerIsVoteOrNot(int $idHackathon, int $idUser): array
{
    $res = array();

    $allProject = getLesProjetProposer($idHackathon);
    $projectVote = getProjetVote($idHackathon, $idUser);

    foreach($allProject as $projet)
    {
        $isVote = false;
        foreach($projectVote as $vote)
        {
            if($projet["id"] == $vote["idprojet"])
            {
                $isVote = true;
                break;
            }
        }

        $projet["isVote"] = $isVote;
        array_push($res, $projet);
    }

    return $res;
}

//Suppression du vote d'un Membre de type Expert sur un projet

function deleteExpertVoteOnProjet($idProjet, $idExpert)
{
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("DELETE FROM vote WHERE idexpert = :idExpert and idprojet = :idProjet;");
        $req->bindParam(':idExpert',$idExpert);
        $req->bindParam(':idProjet',$idProjet);
        $req->execute();
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
}

//Ajout du vote d'un Membre de type Expert sur un projet

function insertVoteExpertOnProjet($idExpert, $idProjet)
{
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("insert into vote(idexpert, idprojet) values (:idExpert, :idProjet)");
        $req->bindParam(':idExpert', $idExpert);
        $req->bindParam(':idProjet', $idProjet);
        $req->execute();
    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
}

//Vérification si le Membre est de type Expert sur un Hackathon

function getIsExpertOnHackathon($idExpert, $idHackathon)
{
    $res = array();
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select idexpert from expertcompose where idexpert = :idExpert and idhackathon = :idHackathon");
        $req->bindParam(":idExpert", $idExpert);
        $req->bindParam(":idHackathon", $idHackathon);
        $req->execute();

        while ($hackathons = $req->fetch())
        {
            $res[] = $hackathons;
        }

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }

    return sizeof($res) == 1;
}

//Vérification si le Membre est de type Animateur sur un Hackathon

function getIsAnimateurOnHackathon($idAnimateur, $idHackathon)
{
    $res = array();
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select idanimateur from animateurcompose where idanimateur = :idAnimateur and idhackathon = :idHackathon");
        $req->bindParam(":idAnimateur", $idAnimateur);
        $req->bindParam(":idHackathon", $idHackathon);
        $req->execute();

        while ($hackathons = $req->fetch())
        {
            $res[] = $hackathons;
        }

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }

    return sizeof($res) == 1;
}

//Récupération du nombre de votes des projets d'un Hackathon

function getNbVoteProjetsByHackathon($idHackathon)
{
    $res = array();
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("select p.id, p.libelle, p.isvalid, h.name, count(v.idexpert) from projet p left outer join vote v on p.id = v.idprojet inner join hackathon h on h.id = p.idhackathon where p.idhackathon = :idHackathon group by p.id, h.name order by count(idExpert) desc");
        $req->bindParam(":idHackathon", $idHackathon);
        $req->execute();

        while ($hackathons = $req->fetch())
        {
            $res[] = $hackathons;
        }

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }

    return $res;
}

//Remise à niveau des phase d'un Hackathon

function setAllClose($id){
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("UPDATE hackathon SET canInscription=0 , canVote=0 , canPosition=0 WHERE id=:id");
        $req->bindParam(':id',$id);
        $req->execute();

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
}

//Ouverture de la phase d'inscription d'un Hackathon

function setInscriptionOpen($id){
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("UPDATE hackathon SET canInscription=1 WHERE id=:id");
        $req->bindParam(':id',$id);
        $req->execute();

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
}

//Ouverture de la phase de vote d'un Hackathon

function setVoteOpen($id) : void
{
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("UPDATE hackathon SET canVote = 1 WHERE id = :id");
        $req->bindParam(':id', $id);
        $req->execute();

    } catch (PDOException $e) {
        echo "Erreur !: " . $e->getMessage();
    }
}

//Ouverture de la phase de posiionnement d'un Hackathon

function setPositionOpen($id){
    try {
        $cnx = connexionPDO();
        $req = $cnx->prepare("UPDATE hackathon SET canposition=1 WHERE id=:id");
        $req->bindParam(':id',$id);
        $req->execute();

    } catch (PDOException $e) {
        print "Erreur !: " . $e->getMessage();
        die();
    }
}

if ($_SERVER["SCRIPT_FILENAME"] == __FILE__) {
    // prog principal de test
    header('Content-Type:text/plain');

    echo "getHackathons() : \n";
    print_r(getHackathons());

//    echo "getHackathon(1) : \n";
//    print_r(getHackathon(1));
//
//    echo "insertRegistration(1,\"John\",\"Doe\")\n";
//    insertRegistration(1,'John', 'Doe');
//
//    echo "getRegistrations(1) : \n";
//    print_r(getInscriptions(1));

}
