<?php
if ( $_SERVER["SCRIPT_FILENAME"] == __FILE__ ){
    $root="..";
}

require_once("$root/model/Project.php");
require_once("$root/dal/bd.project.inc.php");
require_once("$root/dal/bd.equipe.inc.php");
require_once("$root/dal/bd.membre.inc.php");

session_start();

// creation du menu burger
$burgerMenu = array();
$burgerMenu[] = Array("url"=>"./index.php?object=project&action=all","label"=>"Tous");
$burgerMenu[] = Array("url"=>"./index.php?object=project&action=mesProjets","label"=>"Mes Projets");
if(!isset($_SESSION['user'])) {
    $burgerMenu[] = array("url" => "./index.php?object=authentification&action=connexion", "label" => "Connexion");
}else{
    $burgerMenu[] = array("url" => "./index.php?object=authentification&action=deconnexion", "label" => "Déconnexion");
}

// recuperation de l'action
if (isset($_GET["action"])){
    $action = $_GET["action"];
}
else {
    $action = "all";
}

// Gestion des différentes fonctionalités
switch($action) {

    case 'all':
        // Affichage de la liste des projets

        // 1 - Recuperation de la liste à partir de la BDD

        if(!empty($_SESSION['user'])){
            $projectList = getProjects();

            // 2 - Affichage de la liste

            include "$root/view/project/allProject.html.php";
        }else{
            include "$root/view/site/sitePresentation.html.php";
        }



        break;

    case 'mesProjets':

        if(!empty($_SESSION['user'])){
            if($_SESSION['role'] == "Animateur")
            {
                $projectList = $_SESSION['user']->getLesProjectsAnimateur();
            }
            else
            {
                $mesProjects = array();
                $mesProjects = array_merge($_SESSION['user']->getLesProjectsExpert(), $_SESSION['user']->getLesProjectsJury());

                $projectList = $mesProjects;
            }

            $unMembre = $_SESSION['user'];

            include "$root/view/project/allProject.html.php";
        }else{
            include "$root/view/site/sitePresentation.html.php";
        }


        break;

    case 'details':
        // 1 - Recuperation des infos du projet

        if(!empty($_SESSION['user'])){
            $idProject = $_GET['id'];
            $projet = getProject($idProject);
            $equipes =getEquipeByProjet($idProject);

            // 2 - Affichage des infos du projet

            include "$root/view/project/detailsProject.html.php";
        }else{
            include "$root/view/site/sitePresentation.html.php";
        }

        break;

    default:
        include "$root/view/site/sitePresentation.html.php";

}
