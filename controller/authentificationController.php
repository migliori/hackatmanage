<?php

require_once("$root/model/Membre.php");
require_once("$root/dal/bd.authentification.inc.php");

session_start();

$burgerMenu = array();
$burgerMenu[] = Array("url"=>"./index.php?object=site&action=presentation","label"=>"Presentation");

if (isset($_GET["action"])){
    $action = $_GET["action"];
}
else {
    $action = "deconnexion";
}

// Gestion des différentes fonctionalités
switch($action) {

    case 'connexion':
        if(isset($_POST['email']) && isset($_POST['password'])){

            $membre = getMember($_POST['email'],$_POST['password']);
            if($membre != null) {
                if (verifRole($membre)) {
                    $_SESSION['user'] = $membre;

                    if (!isset($_SESSION['user'])) {
                        $burgerMenu[] = array("url" => "./index.php?object=authentification&action=connexion", "label" => "Connexion");
                    } else {
                        $burgerMenu[] = array("url" => "./index.php?object=authentification&action=deconnexion", "label" => "Déconnexion");
                    }

                    include "$root/view/site/sitePresentation.html.php";
                } else {
                    $_SESSION["error"] = "Vous n'avez pas accès à ce site en tant que " . $membre->getFirstname() . " " . $membre->getLastname() ." !";
                    include "$root/view/site/connexion.php";
                }
            }
            else
            {
                $_SESSION["error"] = "Identifiant ou mot de passe incorrect !";
                include "$root/view/site/connexion.php";
            }
        }else{
            include "$root/view/site/connexion.php";
        }

        break;
    case 'deconnexion':
        session_destroy();
        header("Location: ./");
        if(!isset($_SESSION['user'])) {
            $burgerMenu[] = array("url" => "./index.php?object=authentification&action=connexion", "label" => "Connexion");
        }else{
            $burgerMenu[] = array("url" => "./index.php?object=authentification&action=deconnexion", "label" => "Déconnexion");
        }

        include "$root/view/site/sitePresentation.html.php";

    default:
        include "$root/view/site/sitePresentation.html.php";
}
