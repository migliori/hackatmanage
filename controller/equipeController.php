<?php
if ( $_SERVER["SCRIPT_FILENAME"] == __FILE__ ){
    $root="..";
}

require_once("$root/model/Equipe.php");
require_once("$root/dal/bd.project.inc.php");
require_once("$root/dal/bd.equipe.inc.php");
require_once ("$root/dal/bd.membre.inc.php");
require_once("$root/model/Membre.php");
require_once("$root/dal/bd.authentification.inc.php");

session_start();

// creation du menu burger
$burgerMenu = array();
$burgerMenu[] = Array("url"=>"./index.php?object=equipe&action=all","label"=>"Tous");
$burgerMenu[] = Array("url"=>"./index.php?object=equipe&action=mesEquipes","label"=>"Mes Equipes");
if(!isset($_SESSION['user'])) {
    $burgerMenu[] = array("url" => "./index.php?object=authentification&action=connexion", "label" => "Connexion");
}else{
    $burgerMenu[] = array("url" => "./index.php?object=authentification&action=deconnexion", "label" => "Déconnexion");
}

// recuperation de l'action
if (isset($_GET["action"])){
    $action = $_GET["action"];
}
else {
    $action = "all";
}

// Gestion des différentes fonctionalités
switch($action) {

    case 'all':
        // Affichage de la liste des projets

        // 1 - Recuperation de la liste à partir de la BDD
        if(!empty($_SESSION['user'])){
            $equipeList = getEquipes();

            // 2 - Affichage de la liste

            include "$root/view/equipe/allEquipe.html.php";
        }else{
            include "$root/view/site/sitePresentation.html.php";
        }

        break;

    case 'mesEquipes':

        if(!empty($_SESSION['user'])) {
            $mesEquipes = $_SESSION['user']->getLesEquipesAnimateur();

            include "$root/view/equipe/allEquipe.html.php";
        }else{
            include "$root/view/site/sitePresentation.html.php";
        }

        break;

    case 'new':

        $projet = getProject($_GET['id']);

        if(!isset($_GET['nom']))
        {
            include "$root/view/equipe/newEquipeProjet.html.php";
        }
        else
        {
         insertEquipe($_GET['nom'], $projet['id']);
          header('Location:/?object=project&action=details&id='.$projet['id']);
        }

        break;

    case 'details':

        if(!empty($_SESSION['user'])){
            $equipe = getEquipe($_GET['id']);
            if(!empty($equipe)){
                $inscrits = getInscritsEquipe($equipe['id']);
            }


            if(!isset($_GET['idChief']))
            {
                include "$root/view/equipe/detailsEquipe.html.php";
            }
            else
            {
                if(!empty($equipe)){
                    insertChiefIntoEquipe($equipe['id'], $_GET['idChief']);
                }

                header('Location:/?object=equipe&action=details&id='.$equipe['id']);
            }
        }else{
            include "$root/view/site/sitePresentation.html.php";
        }

        break;

    case 'addMembre':
        if(!empty($_SESSION['user'])){
            $idEquipe = $_GET['id'];
            $equipe = getEquipe($idEquipe);
            $projet = getProject($equipe['idprojet']);
            $membres = getMembresWithoutEquipeHackathon($projet['idhackathon']);

            if(!isset($_GET['idMembres']))
            {
                include "$root/view/equipe/addMembreEquipe.html.php";
            }
            else
            {
                foreach ($_GET['idMembres'] as $unIdMembre) {
                    insertMembreIntoEquipe($idEquipe, $unIdMembre);
                    header('Location:/?object=equipe&action=details&id='.$idEquipe);
                }
            }
        }else{
            include "$root/view/site/sitePresentation.html.php";
        }
        break;

    default:
        include "$root/view/site/sitePresentation.html.php";
}
