<?php
if ( $_SERVER["SCRIPT_FILENAME"] == __FILE__ ){
    $root="..";
}

require_once("$root/model/Hackathon.php");
require_once("$root/dal/bd.hackathon.inc.php");
require_once("$root/model/Membre.php");
require_once("$root/dal/bd.membre.inc.php");
require_once("$root/dal/bd.authentification.inc.php");

session_start();

// creation du menu burger
$burgerMenu = array();
$burgerMenu[] = Array("url"=>"./index.php?object=hackathon&action=all","label"=>"Tous");
$burgerMenu[] = Array("url"=>"./index.php?object=hackathon&action=mesHackathons","label"=>"Mes Hackathons");
if(!isset($_SESSION['user'])) {
    $burgerMenu[] = array("url" => "./index.php?object=authentification&action=connexion", "label" => "Connexion");
}else{
    $burgerMenu[] = array("url" => "./index.php?object=authentification&action=deconnexion", "label" => "Déconnexion");
}

// recuperation de l'action
if (isset($_GET["action"])){
    $action = $_GET["action"];
}
else {
    $action = "all";
}

// Gestion des différentes fonctionalités
switch($action) {

    case 'all':

        if(!empty($_SESSION['user'])){
            $hackathonList = getHackathons();

            include "$root/view/hackathon/allHackathon.html.php";
        }else{
            include "$root/view/site/sitePresentation.html.php";
        }
        break;

    case 'mesHackathons':

        if(!empty($_SESSION['user'])){
            if($_SESSION['role'] == "Animateur")
            {
                $hackathonList = $_SESSION["user"]->getLesHackathonsAnimateur();
            }
            else
            {
                $mesHackathons = array();
                $mesHackathons = array_merge($_SESSION["user"]->getLesHackathonsExpert(), $_SESSION["user"]->getLesHackathonsJury());

                $hackathonList = $mesHackathons;
            }

            include "$root/view/hackathon/allHackathon.html.php";
        }else{
            include "$root/view/site/sitePresentation.html.php";
        }
        break;

    case 'details':

        if(!empty($_SESSION['user'])){
            $idHackathon = $_GET['id'];
            $bool = false;

            if($_SESSION["role"] == "Animateur")
            {
                $isExpert = false;
                $isAnimateur = getIsAnimateurOnHackathon($_SESSION["user"]->getId(), $idHackathon);
            }
            else
            {
                $isAnimateur = false;
                $isExpert = getIsExpertOnHackathon($_SESSION["user"]->getId(), $idHackathon);
            }

            if($isAnimateur) {
                $lesProjetsVote = getNbVoteProjetsByHackathon($idHackathon);
                $hackathon = getHackathon($_GET['id']);


                if(isset($_GET['formProject'])) {
                    clearIsValid($idHackathon);
                    header("Location:/?object=hackathon&action=details&id={$hackathon->getId()}");
                }

                $idHackathonState = $_GET['id'];

                if (isset($_GET['idProjet'])) {
                    foreach ($_GET['idProjet'] as $idProjet) {
                        setProjectIsValid($idProjet);
                    }

                    header("Location:/?object=hackathon&action=details&id=$idHackathonState");
                }


                if(isset($_POST['formPhase'])){
                    setAllClose($idHackathonState);
                    header("Location:/?object=hackathon&action=details&id={$hackathon->getId()}");
                }


                if(isset($_POST['isChecked'])){
                    foreach ($_POST['isChecked'] as $check){
                        if ($check == 'vote'){
                            setVoteOpen($idHackathonState);
                        }else if ($check == 'position'){
                            setPositionOpen($idHackathonState);
                        }else{
                            setInscriptionOpen($idHackathonState);
                        }
                    }
                    header("Location:/?object=hackathon&action=details&id=$idHackathonState");
                }
            }

            if($isExpert)
            {
                if(isset($_POST["vote"]))
                {
                    foreach(getProjetVote($idHackathon, $_SESSION["user"]->getId()) as $vote)
                    {
                        deleteExpertVoteOnProjet($vote["idprojet"], $_SESSION["user"]->getId());
                    }

                    foreach($_POST["vote"] as $id => $on)
                    {
                        insertVoteExpertOnProjet($_SESSION["user"]->getId(), $id);
                    }
                }

                $lesProjetProposer = getLesProjetProposerIsVoteOrNot($idHackathon, $_SESSION["user"]->getId());
            }

            $hackathon = getHackathon($idHackathon);
            $inscriptions = getInscriptionsByHackathons($idHackathon);

            // 2 - Affichage des infos du hackathon

            include "$root/view/hackathon/detailsHackathon.html.php";
        }else{
            include "$root/view/site/sitePresentation.html.php";
        }
        break;

    case 'registration':

        // 1 - Recuperation des infos du hackathon
        if(!empty($_SESSION['user'])){
            $hackathon = getHackathon($_GET['id']);

            if(!isset($_GET['firstname']) and isset($_GET['lastname']))
            {
                // 2a - Affichage du formulaire d'inscription

                include "$root/view/hackathon/registrationHackathon.html.php";
            }
            else
            {
                // 2b - Enregistrement des inscription

                insertRegistration($hackathon->getId(),$_GET['firstname'],$_GET['lastname']);
                header('Location:/?object=hackathon&action=details&id='.$hackathon->getId());

            }
        }else{
            include "$root/view/site/sitePresentation.html.php";
        }
        break;

    case 'closeInscription':

        if(!empty($_SESSION['user'])){
            $idHackathon = $_GET['id'];
            $hackathon = getHackathon($idHackathon);

            setInscriptionClose($idHackathon);

            header('Location:/?object=hackathon&action=details&id='.$hackathon->getId());
        }else{
            include "$root/view/site/sitePresentation.html.php";
        }

        break;

    case 'closeVote':

        if(!empty($_SESSION['user'])){
            $idHackathon = $_GET['id'];

            setVoteClose($idHackathon);

            header('Location:/?object=hackathon&action=details&id='.$idHackathon);
        }else{
            include "$root/view/site/sitePresentation.html.php";
        }



        break;

    case 'openVote':

        if(!empty($_SESSION['user'])){
            $idHackathon = $_GET['id'];

            setVoteOpen($idHackathon);

            header('Location:/?object=hackathon&action=details&id='.$idHackathon);
        }else{
            include "$root/view/site/sitePresentation.html.php";
        }



        break;

    case 'closePosition':

        if(!empty($_SESSION['user'])){
            $idHackathon = $_GET['id'];
            $hackathon = getHackathon($idHackathon);

            setPositionClose($idHackathon);

            header('Location:/?object=hackathon&action=details&id='.$hackathon->getId());
        }else{
            include "$root/view/site/sitePresentation.html.php";
        }



        break;

    case 'openPosition':

        if(!empty($_SESSION['user'])){
            $idHackathon = $_GET['id'];
            $hackathon = getHackathon($idHackathon);

            setPositionOpen($idHackathon);

            header('Location:/?object=hackathon&action=details&id='.$hackathon->getId());
        }else{
            include "$root/view/site/sitePresentation.html.php";
        }



        break;

    default:
        include "$root/view/site/sitePresentation.html.php";

}
