<?php
require_once("$root/model/Membre.php");
require_once("$root/dal/bd.authentification.inc.php");

session_start();

if ( $_SERVER["SCRIPT_FILENAME"] == __FILE__ ){
    $root="..";
}

// creation du menu burger
$burgerMenu = array();
$burgerMenu[] = Array("url"=>"./index.php?object=site&action=presentation","label"=>"Presentation");
if(!isset($_SESSION['user'])) {
    $burgerMenu[] = array("url" => "./index.php?object=authentification&action=connexion", "label" => "Connexion");
}else{
    $burgerMenu[] = array("url" => "./index.php?object=authentification&action=deconnexion", "label" => "Déconnexion");
}
// recuperation de l'action
if (isset($_GET["action"])){
    $action = $_GET["action"];
}
else {
    $action = "presentation";
}

// Gestion des différentes fonctionalités
switch($action) {

    case 'presentation':
        // appel des fonctions permettant de recuperer les donnees utiles a l'affichage
        // affichage de la vue
        include "$root/view/site/sitePresentation.html.php";
        break;

    default:
        include "$root/view/site/sitePresentation.html.php";
}
?>
