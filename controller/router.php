<?php

include "getRoot.php";

// Liste des routes

$objects = array();
$objects["site"] = "siteController.php";
$objects["default"] = "siteController.php";
$objects["hackathon"] = "hackathonController.php";
$objects["project"] = "projectController.php";
$objects["authentification"] = "authentificationController.php";
$objects["equipe"] = "equipeController.php";
// Analyse de l'URL

if (isset($_GET["object"]) and array_key_exists($_GET["object"] , $objects )){
    $controller = $objects[$_GET["object"]];

}
else{
    $controller = $objects["default"];
}

// Appel du controleur

include "$root/controller/$controller";
