const checkbox = document.getElementById("phaseProduction"),
    vote = document.getElementById("vote"),
    position = document.getElementById("position"),
    inscription = document.getElementById("inscription");

function validate() {
    if (checkbox.checked) {
        vote.checked = true;
        position.checked = true;
        inscription.checked = true;
    } else {
        vote.checked = false;
        position.checked = false;
        inscription.checked = false;
    }
}

if(vote.checked && position.checked && inscription.checked) {
    checkbox.checked = true;
}