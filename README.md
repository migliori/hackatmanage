# Hackat'Manage

Application destinée aux animateurs et aux experts pour :
- Le choix des projets
- La constitution des équipes

# Langage

- Développée en PHP7

# Base de données

- Postgresql

- Paramètres de connexion à adapter dans le fichier env/db.ini

- Utilisation classe PHP PDO

- Scripts disponibles dans le dossier model/scripts

# Architecture de l'application

- Architecture en MVC : schéma fourni au format draw.io (https://app.diagrams.net/)

# Documentation

- Commentaires de code
- Typage PHP des fonctions
- Versioning
- Dossier doc