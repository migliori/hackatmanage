<?php

class Hackathon
{
    private int $id;
    private string $name;
    private string $topic;
    private string $description;
    private bool $canvote;
    private bool $caninscription;
    private bool $canposition;

    /**
     * @return bool
     */
    public function isCanposition(): bool
    {
        return $this->canposition;
    }

    /**
     * @param bool $canposition
     */
    public function setCanposition(bool $canposition): void
    {
        $this->canposition = $canposition;
    }

    /**
     * @return bool
     */
    public function isCanvote(): bool
    {
        return $this->canvote;
    }

    /**
     * @param bool $canvote
     */
    public function setCanvote(bool $canvote): void
    {
        $this->canvote = $canvote;
    }

    /**
     * @return bool
     */
    public function isCaninscription(): bool
    {
        return $this->caninscription;
    }

    /**
     * @param bool $caninscription
     */
    public function setCaninscription(bool $caninscription): void
    {
        $this->caninscription = $caninscription;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getTopic(): string
    {
        return $this->topic;
    }

    /**
     * @param string $topic
     */
    public function setTopic(string $topic): void
    {
        $this->topic = $topic;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    public function __toString()
    {
        return $this->name;
    }

}
