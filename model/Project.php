<?php

class Project
{
    private int $id;
    private string $libelle;
    private string $description;
    private int $isValid;
    private int $idHackathon;

    /**
     * @return string
     */
    public function getLibelle(): string
    {
        return $this->libelle;
    }

    /**
     * @param string $libelle
     */
    public function setLibelle(string $libelle): void
    {
        $this->libelle = $libelle;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription(string $description): void
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getIsValid(): int
    {
        return $this->isValid;
    }

    /**
     * @param int $isValid
     */
    public function setIsValid(int $isValid): void
    {
        $this->isValid = $isValid;
    }

    /**
     * @return int
     */
    public function getIdHackathon(): int
    {
        return $this->idHackathon;
    }

    /**
     * @param int $idHackathon
     */
    public function setIdHackathon(int $idHackathon): void
    {
        $this->idHackathon = $idHackathon;
    }

    public function __toString()
    {
        return $this->getLibelle();
    }
}