<?php

class Membre{

    private int $id;
    private string $lastname;
    private string $firstname;
    private ?string $phone = null;
    private ?string $email;
    private array $lesHackathonsJury;
    private array $lesHackathonsExpert;
    private array $lesHackathonsAnimateur;
    private array $lesProjectsJury;
    private array $lesProjectsExpert;
    private array $lesProjectsAnimateur;
    private array $lesEquipesAnimateur;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getLastname(): string
    {
        return $this->lastname;
    }

    /**
     * @param string $lastname
     */
    public function setLastname(string $lastname): void
    {
        $this->lastname = $lastname;
    }

    /**
     * @return string
     */
    public function getFirstname(): string
    {
        return $this->firstname;
    }

    /**
     * @param string $firstname
     */
    public function setFirstname(string $firstname): void
    {
        $this->firstname = $firstname;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     */
    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email): void
    {
        $this->email = $email;
    }

    /**
     * @return array
     */
    public function getLesHackathonsJury(): array
    {
        return $this->lesHackathonsJury;
    }

    /**
     * @param array $lesHackathonsJury
     */
    public function setLesHackathonsJury(array $lesHackathonsJury): void
    {
        $this->lesHackathonsJury = $lesHackathonsJury;
    }

    /**
     * @return array
     */
    public function getLesHackathonsExpert(): array
    {
        return $this->lesHackathonsExpert;
    }

    /**
     * @param array $lesHackathonsExpert
     */
    public function setLesHackathonsExpert(array $lesHackathonsExpert): void
    {
        $this->lesHackathonsExpert = $lesHackathonsExpert;
    }

    /**
     * @return array
     */
    public function getLesHackathonsAnimateur(): array
    {
        return $this->lesHackathonsAnimateur;
    }

    /**
     * @param array $lesHackathonsAnimateur
     */
    public function setLesHackathonsAnimateur(array $lesHackathonsAnimateur): void
    {
        $this->lesHackathonsAnimateur = $lesHackathonsAnimateur;
    }

    /**
     * @return array
     */
    public function getLesProjectsJury(): array
    {
        return $this->lesProjectsJury;
    }

    /**
     * @param array $lesProjectsJury
     */
    public function setLesProjectsJury(array $lesProjectsJury): void
    {
        $this->lesProjectsJury = $lesProjectsJury;
    }

    /**
     * @return array
     */
    public function getLesProjectsExpert(): array
    {
        return $this->lesProjectsExpert;
    }

    /**
     * @param array $lesProjectsExpert
     */
    public function setLesProjectsExpert(array $lesProjectsExpert): void
    {
        $this->lesProjectsExpert = $lesProjectsExpert;
    }

    /**
     * @return array
     */
    public function getLesProjectsAnimateur(): array
    {
        return $this->lesProjectsAnimateur;
    }

    /**
     * @param array $lesProjectsAnimateur
     */
    public function setLesProjectsAnimateur(array $lesProjectsAnimateur): void
    {
        $this->lesProjectsAnimateur = $lesProjectsAnimateur;
    }

    /**
     * @return array
     */
    public function getLesEquipesAnimateur(): array
    {
        return $this->lesEquipesAnimateur;
    }

    /**
     * @param array $lesEquipesAnimateur
     */
    public function setLesEquipesAnimateur(array $lesEquipesAnimateur): void
    {
        $this->lesEquipesAnimateur = $lesEquipesAnimateur;
    }

    public function __toString()
    {
        return $this->firstname;
    }

}
