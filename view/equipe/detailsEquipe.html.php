<?php include "$root/view/header.html.php"; ?>
<?php
if($equipe != null){
    ?>

    <div id="accroche"><?php print($equipe['nom']) ?></div>

    <b>Liste des membres de l'équipe :</b>
    <br><br>

    <?php
    $idEquipe = $equipe['id'];

    if($_SESSION['role'] == 'Animateur') {
        $canManage = hasEquipeAnimateur($_SESSION['user'], $idEquipe);
    }
    ?>

    <?php if(sizeof($inscrits) != 0) : ?>
        <form action='/' method='get'>
            <input type='hidden' name='object' value='equipe'>
            <input type='hidden' name='action' value='details'>
            <input type='hidden' name='id' value=<?php echo $idEquipe ?>>

            <table class='bordertable'>
                <tr>
                    <th>Nom</th>
                    <th>Prenom</th>
                    <th>Téléphone</th>
                    <th>Email</th>
                    <th>Chef d'équipe</th>
                </tr>

                <?php
                foreach ($inscrits as $inscrit)
                {
                    $membre = getUser($inscrit['idmembre']);
                    $idInscrit = $inscrit['id'];
                    $isDisabled = $equipe["idchef"] ? "disabled":"";
                    $isChecked = "";

                    if($equipe["idchef"] == $idInscrit)
                    {
                        $isChecked = "checked";
                    }

                    echo "<tr>
                            <td>" . $membre->getLastname() . "</td>
                            <td>" . $membre->getFirstname() . "</td>
                            <td>" . $membre->getPhone() . "</td>
                            <td>" . $membre->getEmail() . "</td>";

                        if($canManage) {
                            echo "<td><input type='radio' name='idChief' value=$idInscrit id=$idInscrit $isDisabled $isChecked></td>";
                        }else {
                            echo "<td><input type='radio' disabled $isChecked></td>";
                        }
                    echo "</tr>";
                }
                ?>
            </table>

            <?php
            if($canManage && $isDisabled != "disabled") {
                echo '<button type="submit">Confirmer mon choix</button>';
            }
            ?>
        </form>

        <?php else : ?>

        <p>Aucun membre dans l'équipe</p>
    <?php endif; ?>
<br>
    <?php
        if($canManage) {
            echo "<a class='textAlignRight' href='./?object=equipe&action=addMembre&id=$idEquipe'>Ajouter un membre à l'équipe</a>";
        }
    ?>

<?php }
else{
    print("<h1 style='display:flex;justify-content: center; align-items: center'>Cette équipe n'existe pas !!</h1>");
}
?>

<?php include "$root/view/footer.html.php"; ?>