<?php include "$root/view/header.html.php"; ?>

    <h1>Ajouter un membre à l'équipe "<?php print($equipe['nom']) ?>"</h1>

    <?php if(sizeof($membres) != 0): ?>

        <form action="/" method="get">

            <input type="hidden" name="object" value="equipe">
            <input type="hidden" name="action" value="addMembre">
            <input type="hidden" name="id" value="<?php print($equipe['id']); ?>">

            <label for="membre">Sélectionner un membre à ajouter</label>
            <select id="membre" name="idMembres[]" multiple>
                <?php
                foreach ($membres as $membre) {
                    echo '<option value='.$membre->getId().'>'.$membre.'</option>';
                }
                ?>
            </select>

            <br/>
            <input type="submit" value="Ajouter">

        </form >
    <?php endif ?>

    <?php
        if(sizeof($membres) == 0)
        {
            echo "Aucun membre à ajouter !";
        }
    ?>

<?php include "$root/view/footer.html.php"; ?>