<?php include "$root/view/header.html.php"; ?>

    <h1>Nouvelle équipe pour le projet "<?php print($projet['libelle']) ?>"</h1>
    <form action="/" method="get">

        <input type="hidden" name="object" value="equipe">
        <input type="hidden" name="action" value="new">
        <input type="hidden" name="id" value="<?php print($projet['id']); ?>">

        <label for="nom">Nom de l'équipe</label><br>
        <input type="text" name="nom" id="nom" required><br>

        <br/>
        <input type="submit" value="Valider">
    </form >

<?php include "$root/view/footer.html.php"; ?>