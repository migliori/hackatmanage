<?php include "$root/view/header.html.php"; ?>
    <?php
        if($_GET['action'] == 'all')
        {
            echo '<div id="accroche">Liste des Équipes</div>';

            echo "<table class='bordertable'>
                    <tr>
                        <th>Nom de l'équipe</th>
                        <th>Projet associé</th>
                        <th>Hackathon</th>
                    </tr>";

            foreach($equipeList as $equipe)
            {
                print("<tr>
                         <td><a class='style' href='./index.php?object=equipe&action=details&id=" . $equipe['id'] . "'>" . $equipe['nom'] . "</a></td>
                         <td>" . getProject($equipe['idprojet'])['libelle'] . "</td>
                         <td>" . getHackathon(getProject($equipe['idprojet'])['idhackathon']) . "</td>
                       </tr>");
            }
            echo '</table>';
        }
        else
        {
            echo '<div id="accroche">Liste des mes Équipes</div>';
            if(!empty($mesEquipes))
            {

                echo "<table class='bordertable'>
                        <tr>
                            <th>Nom de l'équipe</th>
                            <th>Projet associé</th>
                            <th>Hackathon</th>
                        </tr>";

                foreach ($mesEquipes as $equipe)
                {
                    print("<tr>
                         <td><a class='style' href='./index.php?object=equipe&action=details&id=" . $equipe['id'] . "'>" . $equipe['nom'] . "</a></td>
                         <td>" . getProject($equipe['idprojet'])['libelle'] . "</td>
                         <td>" . getHackathon(getProject($equipe['idprojet'])['idhackathon']) . "</td>
                       </tr>");
                }
                echo '</table>';
            }
            else
            {
                echo "<h2>Aucune équipe assignée</h2>";
            }
        }
    ?>

<?php include "$root/view/footer.html.php"; ?>