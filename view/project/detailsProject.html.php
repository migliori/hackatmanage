<?php include "$root/view/header.html.php"; ?>

<?php
if($projet != null){
    ?>

    <div id="accroche"><?php print($projet['libelle']) ?></div>

    <table>
        <?php
        print('<tr><td style="width: 80px; padding-right: 10px">Description : </td><td>' . $projet['description'] . '</td></tr>');
        ?>
    </table>

<br/>
    <b>Liste des équipes :</b>
<br><br>
    <?php
        if($_SESSION["role"] == "Animateur") {
            $canManage = hasProjectAnimateur($_SESSION['user'], $projet['id']);
        }else{
            $canManage = false;
        }

        if(sizeof($equipes) != 0) {
            echo '<table class="bordertable">
                     <tr>
                        <th>id</th>
                        <th>nom</th>
                     </tr>';

                    foreach ($equipes as $e) {
                        print("<tr>
                                 <td>" . $e["id"] . "</td>" .
                                "<td>" . $e["nom"] . "</td>" .
                              "</tr>");
                        }
            echo '</table>';
        }
        else{
            echo 'Aucune équipe';
        }
    ?>
<br>
<?php
    if($canManage) {
        echo '<a class="textAlignRight" href = "./?object=equipe&action=new&id='.$projet['id'].'" > Nouvelle équipe </a >';
    }
?>
<?php }
else{
    print("<h1 style='display:flex;justify-content: center; align-items: center'>Ce projet n'existe pas !!</h1>");
}
?>
<?php include "$root/view/footer.html.php"; ?>