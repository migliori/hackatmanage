<?php include "$root/view/header.html.php"; ?>

    <?php
        if($_GET['action'] == "mesProjets")
        {
            echo '<div id="accroche">Projets de mes Hackathons</div>';

            if(sizeof($projectList) != 0)
            {
                echo "<table class='bordertable'>
                    <tr>
                        <th>Nom du projet</th>                        
                        <th>Hackathon</th>
                    </tr>";

                foreach ($projectList as $project) {
                    if ($project != array()) {
                        print("<tr>
                         <td><a class='style' href='./index.php?object=project&action=details&id=" . $project->getId() . "'>" . $project->getLibelle() . "</a></td>                         
                         <td>" . getHackathon(getProject($project->getId())['idhackathon']) . "</td>
                      </tr>");
                    }
                }
                echo '</table>';
            }
            else
            {
                echo "<h2>Aucun projet assigné</h2>";
            }
        }
        else
        {
            echo '<div id="accroche">Tous les Projets</div>';

            echo "<table class='bordertable'>
                    <tr>
                        <th>Nom du projet</th>                        
                        <th>Hackathon</th>
                    </tr>";

            foreach ($projectList as $project)
            {
                if($project != array()) {
                    print("<tr>
                         <td><a class='style' href='./index.php?object=project&action=details&id=".$project->getId()."'>".$project->getLibelle()."</a></td>                         
                         <td>" . getHackathon(getProject($project->getId())['idhackathon']). "</td>
                      </tr>");
                }
            }
            echo '</table>';
        }
    ?>

<?php include "$root/view/footer.html.php";