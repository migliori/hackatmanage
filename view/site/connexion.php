<?php include "$root/view/header.html.php";

    $message = "";

    if(isset($_SESSION["error"]))
    {
        $message = $_SESSION["error"];
        unset($_SESSION["error"]);
    }
?>

    <div id="accroche">Connexion</div>

    <h2 class="error"><?= $message ?></h2>

    <div class="display-row">
        <form class="display-column" action="./index.php?object=authentification&action=connexion" method="POST">

            <label for="email">Email</label>
            <input type="email" name="email" id="email" required>

            <label for="password">Password</label>
            <input type="password" name="password" id="password" required>

            <button type="submit">Connexion</button>
        </form>
    </div>


<?php include "$root/view/footer.html.php";
