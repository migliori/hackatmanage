<?php include "$root/view/header.html.php"; ?>

    <h1>Nouvelle inscription pour le hackathon <?php print($hackathon->getName()) ?></h1>
    <form action="/" method="get">

        <input type="hidden" name="object" value="hackathon">
        <input type="hidden" name="action" value="registration">
        <input type="hidden" name="id" value="<?php print($hackathon->getId()); ?>">

        <label for="firstname">Prénom</label><br>
        <input type="text" name="firstname" id="firstname" size="20"><br>

        <label for="lastname">Nom</label><br>
        <input type="text" name="lastname" id="lastname" size="20"><br>

        <input type="submit" value="Valider">
    </form >

<?php include "$root/view/footer.html.php";
