<?php include "$root/view/header.html.php"; ?>

<?php
if($hackathon != null){
?>
    <div id="accroche"><?php print($hackathon->getName()) ?></div>

    <table>
        <?php

        $etatVote = "Fermé";
        if($hackathon->isCanvote())
        {
            $etatVote = "Ouvert";
        }

        $etatInscription = "Fermé";
        if($hackathon->isCaninscription())
        {
            $etatInscription = "Ouvert";
        }

        $etatPosition = "Fermé";
        if($hackathon->isCanposition())
        {
            $etatPosition = "Ouvert";
        }

        if($isAnimateur)
        {
            $canManage = hasHackathonAnimateur($_SESSION['user'], $hackathon->getId());
        }

        echo '<ul>
                <li><b>Topic : </b>' . $hackathon->getTopic() . '</li>
                <li><b>Description : </b>' . $hackathon->getDescription() . '</li>
                <li><b>Etat des votes : '.$etatVote.'</b> | <b>Etat des Inscriptions : '.$etatInscription.'</b> | <b>Etat de la phase de positionnement : '.$etatPosition.'</b></li>
                
              </ul>';
        ?>
    </table>
<br>
    <b>Liste des inscrits :</b>

    <?php if(sizeof($inscriptions) == 0) {echo "<br/>Aucun inscrit";} ?>

<?php if(sizeof($inscriptions) != 0): ?>
<br><br>
    <table class="bordertable">
        <tr>
            <th>nom</th>
            <th>prenom</th>
            <th>email</th>
            <th>phone</th>
            <th>date d'inscription</th>
            <th>etat</th>
        </tr>
        <?php foreach($inscriptions as $r) : ?>
            <tr>
                <td> <?php echo $r["lastname"] ?> </td>
                <td> <?php echo $r["firstname"] ?> </td>
                <td> <?php echo  $r["email"] ?> </td>
                <td> <?php echo $r["phone"] ?> </td>
                <td> <?php echo $r["dateinscription"] ?> </td>
                <td> <?php echo $r["isvalid"] ? "pas encore validé" : "validé" ?> </td>
            </tr>
        <?php endforeach; ?>
    </table>
<?php endif ?>

    <br/>
    <br/>
    <?php if($isAnimateur): ?>

        <form action="/" method="get">
            <input type="hidden" name="object" value="hackathon">
            <input type="hidden" name="action" value="details">
            <input type="hidden" name="id" value=<?php echo $hackathon->getId() ?>>
            <input type="hidden" name="formProject">
            <table class="bordertable">
                <tr>
                    <th>id</th>
                    <th>libelle</th>
                    <th>nom hackathon</th>
                    <th>nombre vote</th>
                    <th>projet retenu</th>
                </tr>
                <tr>
                    <?php
                    foreach($lesProjetsVote as $r)

                    {
                        $idProjet = $r["id"];
                        $isChecked = "";

                        if($r["isvalid"] == 1)
                        {
                            $isChecked = "checked";
                        }

                        echo "<tr>
                                 <td>" . $idProjet . "</td>
                                 <td>" . $r["libelle"] . "</td>
                                 <td>" . $r["name"] . "</td>
                                 <td>" . $r["count"] . "</td>";

                        if(!$hackathon->isCanvote() && $canManage)
                        {
                            echo "<td><input type='checkbox' name='idProjet[]' value=$idProjet id=$idProjet $isChecked></td>";
                        }
                        else
                        {
                            echo "<td><input type='checkbox' disabled $isChecked></td>";
                        }
                        echo "</tr>";
                    }
                    ?>
                </tr>
            </table>

            <?php if(!$hackathon->isCanvote() && $canManage && $isChecked != "disabled") : ?>
                <p>Les votes sont actuellement fermés.<br/>Vous pouvez séléctionner, à présent, des projets à retenir si besoin.</p>
                <button type="submit">Confirmer mon choix</button>
                <hr/>
            <?php endif; ?>
        </form>
    <?php endif ?>

    <?php if($isExpert): ?>
        <b>Liste des projets proposé :</b>
        <form action="/index.php?object=hackathon&action=details&id=<?php echo $hackathon->getId()?>" method="post">
            <table class="bordertable">
                <tr>
                    <th>id</th>
                    <th>libelle</th>
                    <th>nom hackathon</th>
                    <?php if($hackathon->isCanvote()){
                        echo '<th>vote</th>';
                    }
                    ?>
                    <th>projet retenu</th>
                </tr>
                <tr>
                    <?php
                        foreach($lesProjetProposer as $r)
                        {
                            $isChecked = "";
                            $isVote = $r["isVote"] ? "checked":"";

                            if($r['isvalid'] == 1)
                            {
                                $isChecked = "checked";
                            }

                            echo "<tr>
                                     <td>" . $r["id"]. "</td>".
                                    "<td>" . $r["libelle"]. "</td>".
                                    "<td>" . $r["name"]. "</td>";
                                    if ($hackathon->isCanvote())
                                    {
                                        echo "<td><input name='vote[" . $r["id"] . "]' type='checkbox' ". $isVote . "></td>";
                                    }
                            echo    "<td><input type='checkbox' disabled $isChecked></td>".
                                 "</tr>";
                        }
                    ?>
                </tr>
            </table>
            <?php if ($hackathon->isCanvote()) : ?>
                <button type="submit">Voté</button>
            <?php endif ; ?>
        </form>
    <?php endif ?>
<br>
<?php
    if($isAnimateur) {

        function isChecked(bool $state){

            if ($state){
                $result = 'checked';
            }else{
                $result = '';
            }
            return $result;
        }

        $isVote = $hackathon->isCanvote();
        $isInscription = $hackathon->isCaninscription();
        $isPosition = $hackathon->isCanposition();

        echo "<form action="."/index.php?object=hackathon&action=details&id={$hackathon->getId()}"." method='post'>
                <input type='hidden' name='formPhase'>
                <tr>
                    <td><input type='checkbox' name='isChecked[]' value='vote' ".isChecked($isVote)." id='vote'><label for='vote'>Etat des votes</label></td>
                    <td><input type='checkbox' name='isChecked[]' value='position' ".isChecked($isPosition)." id='position'><label for='position'>Etat de la phase de positionnement</label></td>
                    <td><input type='checkbox' name='isChecked[]' value='inscription' ".isChecked($isInscription)." id='inscription'><label for='inscription'>Etat des inscriptions</label></td>
                    <td><input type='checkbox' id='phaseProduction' onclick='validate()'><label for='phaseProduction'>Etat phase de production</label></td>

                    <td><button type='submit'>Confirmer</button></td>
                </tr>
              <form>";
    }
?>
<script src="/public/js/phaseProduction.js"></script>

<?php }
else{
    print("<h1 style='display:flex;justify-content: center; align-items: center'>Ce hackathon n'existe pas !!</h1>");
}
?>

<?php include "$root/view/footer.html.php"; ?>
